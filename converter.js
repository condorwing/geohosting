var deg2rad = Math.PI / 180,
    wgs84e = 0.0818191908426,
    radius = 6378137,
    equator = 40075016.685578488,
    zMax = 23,
    pixelWorldSize = Math.pow(2, zMax + 8),
    a = pixelWorldSize / equator,
    b = .5 * equator,
    quarterPi = Math.PI / 4;

function sphericalMercatorToPixels (coords) {
    var lat = coords[0] * deg2rad,
        lon = coords[1] * deg2rad,
        tan = Math.tan(quarterPi + lat / 2.0);
    
    return [
        a * (b + radius * lon),
        a * (b - radius * Math.log(tan))
    ];
}

function wgs84MercatorToPixels (coords) {
    var lat = coords[0] * deg2rad,
        lon = coords[1] * deg2rad,
        tan = Math.tan(quarterPi + lat / 2.0),
        esinLat = wgs84e * Math.sin(lat),
        pow = Math.pow(Math.tan(quarterPi + .5 * Math.asin(esinLat)), wgs84e);
    
    return [
        a * (b + radius * lon),
        a * (b - radius * Math.log(tan / pow))
    ];
}

exports.tileToPrefix = function (x, y, z) {
    var scale = Math.pow(2, zMax - z);
    return pixelsToPrefix([x * scale * 256, y * scale * 256]);
}

exports.geoToPixels = function (coords, useSpericalProjection) {
    return useSpericalProjection ? sphericalMercatorToPixels(coords) : wgs84MercatorToPixels(coords);
};

var pixelsToPrefix = exports.pixelsToPrefix = function (pixels) {
    var res = 0,
        x = parseInt(pixels[0]),
        y = parseInt(pixels[1]);
    
    for (var i = 0; i < zMax; i++) {
        res = (res + (x % 2) * 2 + (y % 2)) * 4;
        x = Math.floor(.5 * x);
        y = Math.floor(.5 * y);
    }
    
    return res;
}