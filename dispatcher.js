var converter = require('./converter'),
    formidable = require('formidable'),
    vow = require('vow'),
    url = require('url');

exports.process = function (config, services, request) {
    return dispatch({
        config: config,
        request: request,
        query: url.parse(request.url, true).query,
        converter: converter,
        dataSource: services.dataSource,
        dataCache: services.dataCache
    });
};

function dispatch (app) {
    var config = app.config,
        request = app.request,
        action = config.actions[app.query.action],
        waiting = [],
        result = vow.promise();

    if (action) {
        if (action.upload && app.request.method.toLowerCase() == 'post') {
            var uploadPromise = vow.promise();
            waiting.push(uploadPromise);

            var form = new formidable.IncomingForm();
            form.parse(request, function (e, fields, files) {
                if (e) {
                    uploadPromise.reject(e);
                } else {
                    for (name in fields) {
                        app.query[name] = fields[name];
                    }
                    for (file in files) {
                        app.query[action.upload] = JSON.parse(fs.readFileSync(files[file].path));
                    }

                    uploadPromise.fulfill();
                }
            });
        }

        if (waiting.length) {
            vow.all(waiting).then(onReady, function (e) { result.reject(e) });
        } else {
            onReady();
        }

        function onReady () {
            require(action.module).process(app)/*.timeout(action.timeout || config.timeout)*/.then(
                function (html) {
                    result.fulfill(html);
                },
                function (e) {
                    result.reject(e);
                }
            )
        }
    } else {
        result.reject({ code: 400, message: 'Unknown action' });
    }

    return result;
}