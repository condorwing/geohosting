var mysql = require('mysql'),
    vow = require('vow');

exports.create = function (options) {
    var source = new MysqlDataSource(options),
        result = vow.promise();

    source.connect().then(function () {
        result.fulfill(source);
    }, result.reject);

    return result;
}

var MysqlDataSource = function (options) {
        this.options = require('node.extend')({}, options, {
            multipleStatements: true
        });
    };

MysqlDataSource.prototype.connect = function () {
    var result = vow.promise();

    this.connection = mysql.createConnection(this.options);
    this.connection.connect((function (e) {
        if (e) {
            result.reject(e);
        } else {
            this.connection.query('SET CHARACTER SET utf8', function (e) {
                if (e) {
                    result.reject(e);
                } else {
                    result.fulfill();
                }
            });
        }
    }).bind(this));

    return result;
}

MysqlDataSource.prototype.insert = function (app, features) {
    var result = vow.promise(),
        chunkSize = this.options.chunkSize || 1000,
        query = [];

    for (var i = 0; i < features.length; i++) {
        var feature = features[i];

        query.push([
            'INSERT INTO features (id, geometry, title, properties, options) VALUES (',
            [
                feature.id,
                JSON.stringify(feature.geometry),
                feature.title,
                JSON.stringify(feature.properties),
                JSON.stringify(feature.options)
            ].map(mysql.escape).join(', '),
            ')'
        ].join(''));

        query.push([
            'INSERT INTO tiles (tile, feature_id) VALUES (',
            [ feature.tile, feature.id ].map(mysql.escape).join(', '),
            ')'
        ]);
    }

    result.fulfill({
        status: 200,
        message: JSON.stringify(query)
        //message: feature.length + ' rows inserted successfully'
    });
    
    return result;
};
/*
MysqlDataSource.prototype.insert = function (features, callback) {
    var converter = require('./../converter');
    
    next.call(this);
    
    function next () {
        var feature = features.shift();
        
        if (feature) {
            this._query("INSERT INTO features (id, geometry, title, properties, options) VALUES (" +
                "':id', ':geometry', ':title', ':properties', ':options'" +
            ")", {
                id: feature.id,
                geometry: JSON.stringify(feature.geometry),
                title: feature.title,
                properties: JSON.stringify(feature.properties),
                options: JSON.stringify(feature.options)
            }, (function (e) {
                if (e) {
                    callback(e);
                } else {
                    var tile = converter.pixelsToPrefix(converter.geoToPixels(feature.geometry.coordinates));
                    
                    this._query("INSERT INTO tiles (tile, feature_id) VALUES (':tile', ':id')", {
                        tile: tile,
                        id: feature.id
                    }, (function (e) {
                        if (e) {
                            callback(e);
                        } else {
                            next.call(this);
                        }
                    }).bind(this));
                }
            }).bind(this));
        } else {
            callback();
        }
    }
};

MysqlDataSource.prototype.getTile = function (prefix, callback) {
    var mask = getMask(prefix),
        gridMask = mask << 16;

    this._query(
        "SELECT COUNT(*) AS `number`, (tile & :gridMask) >> 16 AS `grid` FROM tiles WHERE (tile & :mask) = :prefix GROUP BY `grid`", {
            mask: mask,
            gridMask: gridMask,
            prefix: prefix
        },
        callback
    );
};

var powers = [],
    count = 1;
for (var i = 0; i < 31; i++) {
    powers.push(count - 1);
    count *= 2;
}

function getMask (prefix) {
    var i = 0;
    while (powers[i] < prefix) {
        i++;
    }
    return powers[i];
}

function createConnectionSync (options) {
    return require('mysql-libmysqlclient').createConnectionSync(
        options.host,
        options.username,
        options.password,
        options.database
    );
}

MysqlDataSource.prototype._testConnectionSync = function () {
    if (!this.connection || !this.connection.connectedSync()) {
        this.connection = createConnectionSync(this.options);
    }
    return this.connection.connectedSync();
}

MysqlDataSource.prototype._query = function (query, params, callback) {
    var preparedQuery = this._prepareQuerySync(query, params);
if (typeof params.prefix != 'undefined') callback(preparedQuery);
    this.connection.query(preparedQuery, (function (e, res) {
        if (e) {
            callback(e + ' in query ' + preparedQuery);
        } else {
            callback(null, res);
        }
    }).bind(this));
}

MysqlDataSource.prototype._prepareQuerySync = function (query, params) {
    var match,
        search = /\:(\w+)/g,
        preparedQuery = query;

    while (match = search.exec(query)) {
        if (typeof params[match[1]] == 'undefined') {
            throw new Error (JSON.stringify(params) + ': ' + match[1]);
        }
        preparedQuery = preparedQuery.replace(match[0], this.connection.escapeSync(params[match[1]].toString()));
    }
    
    return preparedQuery;
}

MysqlDataSource.prototype.get = function (bounds, callback) {
    if (!this._testConnectionSync()) {
        callback('MysqlDataSource.get: cannot connect');
    } else {
        callback(null);
    }
}

*/