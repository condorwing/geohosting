var converter = require(__dirname + '/../converter.js');

exports.process = function (app) {
    var x = app.query.x,
        y = app.query.y,
        z = app.query.z,
        tilePrefix = converter.tileToPrefix(x, y, z);

    app.dataSource.getTile(tilePrefix, function (e, res) {
        if (e) {
            app.error(e);
        } else {
            app.success(JSON.stringify(res.fetchAllSync(), null, 4));
        }
    });
};