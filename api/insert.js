var vow = require('vow');

exports.process = function (app) {
    var data = app.query.data,
        result = vow.promise(),
        inserted = 0;
    
    if (!data) {
        result.reject({ code: 400, message: '"data" parameter required' });
        return result;
    }
    
    try {
        data = JSON.parse(data);
    } catch (e) {
        result.reject({ code: 400, message: 'Cannot parse data' });
        return result;
    }
    
    if (!data || data.type != 'FeatureCollection' || !data.features || !data.features.length) {
        result.reject({ code: 400, message: 'Bad data format' });
        return result;
    }

    var features = data.features,
        total = features.length;
    
    result.sync(insert(app, features));

    return result;
};

function insert (app, data) {
    var total = data.length,
        result = vow.promise(),
        features = [];

    for (var i = 0; i < total; i++) {
        var feature = data[i],
            id = feature && feature.properties && feature.properties.id;
        
        if (typeof id != 'number') {
            result.reject('Feature #' + (i + 1) + ': no id in properties or id is not a number');
            return result;
        }
        
        var geometry = feature.geometry;
        if (!geometry || geometry.type != 'Point' || !geometry.coordinates || geometry.coordinates.length < 2) {
            result.reject('Feature "' + id + '": bad geometry format');
            return result;
        }
        
        var title = feature.properties.title;
        if (!title) {
            result.reject('Feature "' + id + '": no title property found');
            return result;
        }

        features.push({
            id: id,
            tile: app.converter.pixelsToPrefix(app.converter.geoToPixels(geometry.coordinates)),
            geometry: geometry,
            title: title,
            properties: feature.properties,
            options: feature.options || {}
        });
    }
    
    result.sync(app.dataSource.insert(app, features));

    return result;
}
