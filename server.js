var vow = require('vow'),
    domain = require('domain');

exports.create = function (config) {
    var result = vow.promise(),
        commonDomain = domain.create(),
        services = {},
        stop = function () {
            commonDomain.dispose();
            if (services.httpServer) {
                services.httpServer.close();
            }
            if (services.dataSource) {
                services.dataSource.stop();
            }
            if (services.dataCache) {
                services.dataCache.stop();
            }
        },
        onError = function (e) {
            if (!result.isResolved()) {
                result.reject(e);
            }
            if (config.stderr) {
                config.stderr.write(e.toString());
            }
        };

    commonDomain.on('error', onError);

    commonDomain.run(function () {
        vow.all({
            dataSource: createDataSource(),
            dataCache: createDataCache(),
            httpServer: createHttpServer()
        }).then(function (res) {
            services.httpServer = res.httpServer;
            services.dataSource = res.dataSource;
            services.dataCache = res.dataCache;
            services.httpServer.listen(config.port);

            result.fulfill({ stop: stop });
        }, onError);
    });

    function createDataSource () {
        return require(config.dataSource.module).create(config.dataSource.options);
    }

    function createDataCache () {
        return require(config.dataCache.module).create(config.dataCache.options);
    }

    function createHttpServer () {
        var dispatcher = require(config.dispatcher.module);

        return vow.promise(require('http').createServer(function (request, response) {
            var requestDomain = domain.create(),
                onRequestError = function (e) {
                    try {
                        response.writeHead(
                            config.debug ? (e && e.code || 500) : 200,
                            { 'Content-Type': 'text/plain; charset=utf-8' }
                        );
                        response.end(config.debug ? (e.message ? e.message.toString() : e.toString()) : '');
                        response.on('close', function () {
                            requestDomain.dispose();
                        });
                    } catch (er) {
                        requestDomain.dispose();
                    }
                    onError(e);
                }
            requestDomain.on('error', onRequestError);

            requestDomain.run(function () {
                dispatcher.process(config, services, request).then(
                    function (html) {
                        try {
                            response.writeHead(200);
                            response.end(typeof html == 'string' ? html : JSON.stringify(html));
                            response.on('close', function () {
                                requestDomain.dispose();
                            });
                        } catch (e) {
                            onError(e);
                        }
                    },
                    onRequestError
                );
            });
        }));
    }

    return result;
};
